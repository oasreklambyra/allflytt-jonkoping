'use strict';

$ = jQuery;

var Input = React.createClass({
	displayName: 'Input',

	render: function render() {
		var label = null;
		if (this.props.labelName && this.props.labelName.length > 0) {
			label = React.createElement(
				'label',
				{ htmlFor: this.props.idName },
				this.props.labelName
			);
		}

		var inputAddon = null;
		if (this.props.inputAddon && this.props.inputAddon.length > 0) {
			var type = this.props.inputAddonType === 'square' ? React.createElement(
				'sup',
				null,
				'2'
			) : '';
			inputAddon = React.createElement(
				'div',
				{ className: 'input-group-addon' },
				this.props.inputAddon,
				type
			);
		}

		var desc = null;
		if (this.props.desc && this.props.desc.length > 0) {
			desc = React.createElement(
				'div',
				{ className: 'input-group-addon', style: { padding: '0' } },
				React.createElement(
					'div',
					{ 'data-toggle': 'popover', 'data-placement': 'top', 'data-content': this.props.desc, style: { cursor: 'pointer', padding: '6px 12px' } },
					React.createElement('i', { className: 'fa fa-info' })
				)
			);
		}

		var wrapperClass = 'form-group';
		if (this.props.error && this.props.error.length > 0) {
			wrapperClass += ' ' + 'has-error';
		}

		return React.createElement(
			'div',
			{ className: wrapperClass },
			label,
			React.createElement(
				'div',
				{ className: 'input-group', style: { width: '100%' } },
				inputAddon,
				React.createElement('input', { id: this.props.idName,
					className: 'form-control',
					type: this.props.type,
					value: this.props.value,
					onChange: this.props.onChange,
					name: this.props.idName,
					placeholder: this.props.placeholder
				}),
				desc
			),
			React.createElement(
				'div',
				{ className: 'text-danger' },
				React.createElement(
					'strong',
					null,
					this.props.error
				)
			)
		);
	}
});

var Checkbox = React.createClass({
	displayName: 'Checkbox',

	componentWillMount: function componentWillMount() {
		this.props.data[this.props.idName] = this.props['default'];
	},
	render: function render() {

		return React.createElement(
			'label',
			{ className: this.props.className},
			React.createElement('input', { id: this.props.idName,
				type: 'checkbox',
				name: this.props.idName,
				onChange: this.props.onChange,
				value: this.props.value
			}),
			React.createElement(
				'span',
				null,
				this.props.label
			),
			React.createElement(
				'div',
				{ className: 'text-danger' },
				React.createElement(
					'strong',
					null,
					this.props.error
				)
			)
		);
	}
});

var Select = React.createClass({
	displayName: 'Select',

	currentOption: 0,
	componentWillMount: function componentWillMount() {
		this.props.data[this.props.idName] = this.props['default'];
	},
	shouldComponentUpdate: function shouldComponentUpdate() {
		if (this.currentOption === this.props.data[this.props.idName]) {
			return false;
		} else {
			return true;
		}
	},
	render: function render() {
		var options = [];
		this.currentOption = this.props.data[this.props.idName] ? this.props.data[this.props.idName] : this.props['default'];
		this.props.options.forEach(function (option) {
			options.push(React.createElement(
				'option',
				{ value: option.value, key: option.value },
				option.label
			));
		});
		return React.createElement(
			'div',
			{ className: 'form-group' },
			React.createElement(
				'label',
				{ htmlFor: this.props.idName },
				this.props.labelName
			),
			React.createElement(
				'select',
				{ className: 'form-control',
					id: this.props.idName, name: this.props.idName,
					onChange: this.props.onChange,
					value: this.currentOption
				},
				options
			)
		);
	}
});

var Radio = React.createClass({
	displayName: 'Radio',

	currentChecked: '1',
	componentWillMount: function componentWillMount() {
		if (this.props.identifier === '1') {
			this.props.data[this.props.idName] = this.props.value;
		}
	},
	shouldComponentUpdate: function shouldComponentUpdate() {
		if (this.currentChecked === this.props.data[this.props.idName]) {
			return false;
		} else {
			return true;
		}
	},
	render: function render() {
		this.currentChecked = this.props.data[this.props.idName] === this.props.value ? this.props.identifier : false;
		var identifier = this.props.idName + this.props.identifier;
		return React.createElement(
			'label',
			{ className: this.props.className },
			React.createElement('input', {
				id: identifier,
				type: 'radio',
				checked: this.currentChecked === this.props.identifier,
				name: this.props.idName,
				onChange: this.props.onChange,
				value: this.props.value
			}),
			React.createElement(
				'span',
				null,
				this.props.label
			)
		);
	}
});

/* Contains the Form */
var Form = React.createClass({
	displayName: 'Form',

	showPackingHelp: false,
	showNewAddressFields: true,
	shouldComponentUpdate: function shouldComponentUpdate() {
		if (this.props.data.packingHelp === "1") {
			this.showPackingHelp = true;
		} else {
			this.showPackingHelp = false;
		}

		if (this.props.data.useMagazine === true) {
			this.showNewAddressFields = false;
		} else {
			this.showNewAddressFields = true;
		}
		return true;
	},
	getNewAddressFields: function getNewAddressFields() {
		return React.createElement(
			'div',
			{ className: 'col-sm-12', style: { border: '1px solid #CCC', backgroundColor: '#FFF', marginBottom: '20px', padding: '15px' } },
			React.createElement(
				'h2',
				{ style: { textTransform: 'uppercase', marginBottom: '20px', marginTop: '30px' } },
				'Nya boendeuppgifter'
			),
			React.createElement(
				'div',
				{ className: 'row' },
				React.createElement(
					'div',
					{ className: 'col-sm-4' },
					React.createElement(Input, {
						type: 'text',
						labelName: 'Adress',
						idName: 'newCustomerAddress',
						error: this.props.errors.newCustomerAddress,
						placeholder: 'Adress.',
						desc: 'Ange den nya adressen.',
						data: this.props.data,
						onChange: this.props.onChange
					}),
					React.createElement(Input, {
						type: 'text',
						labelName: 'Postnummer',
						idName: 'newCustomerPostal',
						error: this.props.errors.newCustomerPostal,
						placeholder: 'Postnummer.',
						desc: 'Ange din nya postadress.',
						data: this.props.data,
						onChange: this.props.onChange
					}),
					React.createElement(Input, {
						type: 'text',
						labelName: 'Ort',
						idName: 'newCustomerCity',
						error: this.props.errors.newCustomerCity,
						placeholder: 'Stad',
						desc: 'Ange staden som den nya adressen ligger i.',
						data: this.props.data,
						onChange: this.props.onChange
					})
				),
				React.createElement(
					'div',
					{ className: 'col-sm-4' },
					React.createElement(Select, {
						labelName: 'Bostadstyp',
						onChange: this.props.onChange,
						data: this.props.data,
						idName: 'newLivingType',
						error: this.props.errors.newLivingType,
						'default': 'laganhet',
						options: [{
							value: 'laganhet',
							label: 'Lägenhet'
						}, {
							value: 'radhus',
							label: 'Radhus'
						}, {
							value: 'villa',
							label: 'Villa'
						}, {
							value: 'kontor',
							label: 'Kontor'
						}, {
							value: 'magasin',
							label: 'Magasin'
						}, {
							value: 'annat',
							label: 'Annat'
						}] }),
					React.createElement(Select, {
						labelName: 'Våning',
						onChange: this.props.onChange,
						data: this.props.data,
						idName: 'newLevel',
						error: this.props.errors.newLevel,
						'default': '0',
						options: [{
							value: '0',
							label: '0'
						}, {
							value: '1',
							label: '1'
						}, {
							value: '2',
							label: '2'
						}, {
							value: '3',
							label: '3'
						}, {
							value: '4',
							label: '4'
						}, {
							value: '5',
							label: '5'
						}, {
							value: '6',
							label: '6'
						}, {
							value: '7',
							label: '7'
						}, {
							value: 'moreThan7',
							label: 'Fler än 7'
						}] })
				),
				React.createElement(
					'div',
					{ className: 'col-sm-4' },
					React.createElement(Select, {
						labelName: 'Hiss',
						onChange: this.props.onChange,
						data: this.props.data,
						idName: 'newElevator',
						error: this.props.errors.newElevator,
						'default': false,
						options: [{
							value: false,
							label: 'Nej'
						}, {
							value: 'small',
							label: 'Liten hiss'
						}, {
							value: 'large',
							label: 'Stor hiss (4 pers eller fler)'
						}, {
							value: 'unknown',
							label: 'Vet ej'
						}] })
				)
			)
		);
	},
	getPackingHelpElements: function getPackingHelpElements() {
		return React.createElement(
			'div',
			null,
			React.createElement(
				'div',
				{ className: 'form-group' },
				React.createElement(
					'label',
					null,
					'Vilken typ av packningshjälp?'
				),
				React.createElement('br', null),
				React.createElement(Checkbox, {
					label: 'Packning',
					idName: 'packingHelpPacking',
					error: this.props.errors.packingHelpPacking,
					value: 'packing',
					className: 'checkbox-inline',
					onChange: this.props.onChange,
					data: this.props.data,
					'default': false }),
				React.createElement(Checkbox, {
					label: 'Uppackning',
					idName: 'packingHelpUnpacking',
					error: this.props.errors.packingHelpUnpacking,
					value: 'unpacking',
					className: 'checkbox-inline',
					onChange: this.props.onChange,
					data: this.props.data,
					'default': false })
			),
			React.createElement(
				'div',
				{ className: 'form-group' },
				React.createElement(
					'label',
					null,
					'Endast porslin?'
				),
				React.createElement('br', null),
				React.createElement(Checkbox, {
					label: 'Packning',
					idName: 'packingHelpPackingPorcelain',
					error: this.props.errors.packingHelpPackingPorcelain,
					value: 'packing',
					className: 'checkbox-inline',
					onChange: this.props.onChange,
					data: this.props.data,
					'default': false }),
				React.createElement(Checkbox, {
					label: 'Uppackning',
					idName: 'packingHelpUnpackingPorcelain',
					error: this.props.errors.packingHelpUnpackingPorcelain,
					value: 'unpacking',
					className: 'checkbox-inline',
					onChange: this.props.onChange,
					data: this.props.data,
					'default': false })
			),
			React.createElement(
				'div',
				{ className: 'form-group' },
				React.createElement(
					'label',
					null,
					'Lån av flyttkargonger?'
				),
				React.createElement('br', null),
				React.createElement(Radio, {
					label: 'Nej',
					idName: 'packingHelpNeedBoxes',
					error: this.props.errors.packingHelpNeedBoxes,
					className: 'radio-inline',
					onChange: this.props.onChange,
					data: this.props.data,
					identifier: '1',
					value: '0' }),
				React.createElement(Radio, {
					label: 'Ja',
					idName: 'packingHelpNeedBoxes',
					error: this.props.errors.packingHelpNeedBoxes,
					className: 'radio-inline',
					onChange: this.props.onChange,
					data: this.props.data,
					identifier: '2',
					value: '1' })
			),
			React.createElement(Input, {
				type: 'number',
				labelName: 'Hur många kartonger?',
				idName: 'packingHelpBoxes',
				error: this.props.errors.packingHelpBoxes,
				placeholder: 'Antal kartonger',
				desc: 'Ange antalet flyttkargoner som behöver lånas.',
				data: this.props.data,
				onChange: this.props.onChange
			})
		);
	},
	render: function render() {
		var packingHelpElement = '';
		var cleaningCalc = 'help!'

		if (this.showPackingHelp) {
			packingHelpElement = this.getPackingHelpElements();
		}

		var newAddressFields = '';
		if (this.showNewAddressFields) {
			newAddressFields = this.getNewAddressFields();
		}

		return React.createElement(
			'form',
			{ method: this.props.method },
			React.createElement(
				'div',
				{ className: 'col-sm-12', style: { border: '1px solid #CCC', backgroundColor: '#FFF', marginBottom: '20px', padding: '15px' } },
				React.createElement(
					'h2',
					{ style: { textTransform: 'uppercase', marginBottom: '20px', marginTop: '30px' } },
					'Boendeuppgifter'
				),
				React.createElement(
					'div',
					{ className: 'row' },
					React.createElement(
						'div',
						{ className: 'col-sm-4' },
						React.createElement(Input, {
							type: 'text',
							labelName: 'Namn',
							idName: 'customerName',
							error: this.props.errors.customerName,
							placeholder: 'Namn',
							desc: 'Ange ditt för- och efternamn.',
							data: this.props.data,
							onChange: this.props.onChange
						}),
						React.createElement(Input, {
							type: 'text',
							labelName: 'Adress',
							idName: 'customerAddress',
							error: this.props.errors.customerAddress,
							placeholder: 'Adress',
							desc: 'Ange adressen som städningen ska utföras i.',
							data: this.props.data,
							onChange: this.props.onChange
						}),
						React.createElement(Input, {
							type: 'text',
							labelName: 'Postnummer',
							idName: 'customerPostal',
							error: this.props.errors.customerPostal,
							placeholder: 'Postnummer',
							desc: 'Ange ditt postnummer.',
							data: this.props.data,
							onChange: this.props.onChange
						}),
						React.createElement(Input, {
							type: 'text',
							labelName: 'Ort',
							idName: 'customerCity',
							error: this.props.errors.customerCity,
							placeholder: 'Stad',
							desc: 'Ange staden som städningen ska utföras i.',
							data: this.props.data,
							onChange: this.props.onChange
						}),
						React.createElement(Select, {
							labelName: 'Vill ha svar via',
							onChange: this.props.onChange,
							data: this.props.data,
							idName: 'respondTo',
							error: this.props.errors.respondTo,
							'default': 'email',
							options: [{
								value: 'email',
								label: 'E-post'
							}, {
								value: 'phone',
								label: 'Telefon'
							}, {
								value: 'both',
								label: 'E-post \& Telefon'
							}] })
					),
					React.createElement(
						'div',
						{ className: 'col-sm-4' },
						React.createElement(Input, {
							type: 'text',
							labelName: 'Telefon',
							idName: 'customerPhone',
							error: this.props.errors.customerPhone,
							placeholder: 'Telefonnummer',
							desc: 'Ange det telefonnummer som en kontaktperson kan nås på.',
							data: this.props.data,
							onChange: this.props.onChange
						}),
						React.createElement(Input, {
							type: 'email',
							labelName: 'E-post',
							idName: 'customerEmail',
							error: this.props.errors.customerEmail,
							placeholder: 'E-post',
							desc: 'Ange e-postadressen som en kontaktperson kan nås på.',
							data: this.props.data,
							onChange: this.props.onChange
						}),
						React.createElement(Input, {
							type: 'email',
							labelName: 'Bekräfta e-post',
							idName: 'customerEmailCompare',
							error: this.props.errors.customerEmailCompare,
							placeholder: 'Bekräfta e-post',
							desc: 'Ange e-postadressen igen som en kontaktperson kan nås på för att vara säker på att rätt adress har angivits.',
							data: this.props.data,
							onChange: this.props.onChange
						}),
						React.createElement(Select, {
							labelName: 'Bostadstyp',
							onChange: this.props.onChange,
							data: this.props.data,
							idName: 'livingType',
							error: this.props.errors.livingType,
							'default': 'laganhet',
							options: [{
								value: 'laganhet',
								label: 'Lägenhet'
							}, {
								value: 'radhus',
								label: 'Radhus'
							}, {
								value: 'villa',
								label: 'Villa'
							}, {
								value: 'kontor',
								label: 'Kontor'
							}, {
								value: 'magasin',
								label: 'Magasin'
							}, {
								value: 'annat',
								label: 'Annat'
							}] })
					),
					React.createElement(
						'div',
						{ className: 'col-sm-4' },
						React.createElement(Select, {
							labelName: 'Storlek',
							onChange: this.props.onChange,
							data: this.props.data,
							idName: 'size',
							error: this.props.errors.size,
							'default': '1',
							options: [{
								value: '1',
								label: '1 rum och kök'
							}, {
								value: '2',
								label: '2 rum och kök'
							}, {
								value: '3',
								label: '3 rum och kök'
							}, {
								value: '4',
								label: '4 rum och kök'
							}, {
								value: '5',
								label: '5 rum och kök'
							}, {
								value: '6+',
								label: '6 eller fler rum'
							}] }),
						React.createElement(Select, {
							labelName: 'Våning',
							onChange: this.props.onChange,
							data: this.props.data,
							idName: 'level',
							error: this.props.errors.level,
							'default': '0',
							options: [{
								value: '0',
								label: '0'
							}, {
								value: '1',
								label: '1'
							}, {
								value: '2',
								label: '2'
							}, {
								value: '3',
								label: '3'
							}, {
								value: '4',
								label: '4'
							}, {
								value: '5',
								label: '5'
							}, {
								value: '6',
								label: '6'
							}, {
								value: '7',
								label: '7'
							}, {
								value: 'moreThan7',
								label: 'Fler än 7'
							}] }),
						React.createElement(Select, {
							labelName: 'Hiss',
							onChange: this.props.onChange,
							data: this.props.data,
							idName: 'elevator',
							error: this.props.errors.elevator,
							'default': false,
							options: [{
								value: false,
								label: 'Nej'
							}, {
								value: 'small',
								label: 'Liten hiss'
							}, {
								value: 'large',
								label: 'Stor hiss (4 pers eller fler)'
							}, {
								value: 'unknown',
								label: 'Vet ej'
							}] })
					)
				)
			),
			React.createElement(
				'div',
				{ className: 'col-sm-12', style: { border: '1px solid #CCC', backgroundColor: '#FFF', marginBottom: '20px', padding: '15px' } },
				React.createElement(
					'h2',
					{ style: { textTransform: 'uppercase', marginBottom: '20px', marginTop: '30px' } },
					'Flyttuppgifter'
				),
				React.createElement(
					'div',
					{ className: 'row' },
					React.createElement(
						'div',
						{ className: 'col-sm-4' },
						React.createElement(Input, {
							type: 'text',
							labelName: 'Flyttdatum',
							idName: 'movingDate',
							error: this.props.errors.movingDate,
							placeholder: 'T.ex: 2015-09-25',
							desc: 'Ange datum för flytt, följ mönstret år-månad-dag.',
							data: this.props.data,
							onChange: this.props.onChange
						}),
						React.createElement(Input, {
							type: 'number',
							labelName: 'Boyta',
							idName: 'boyta',
							error: this.props.errors.boyta,
							placeholder: 'Ange boyta',
							desc: 'Ange kvadratmeter för den totala boytan.',
							data: this.props.data,
							onChange: this.props.onChange,
							inputAddon: 'm',
							inputAddonType: 'square'
						}),
						React.createElement(Input, {
							type: 'number',
							labelName: 'Biyta',
							idName: 'biyta',
							error: this.props.errors.biyta,
							placeholder: '(källare, förråd)',
							desc: 'Ange kvadratmeter för totala biytan, så som källare och/eller förråd.',
							data: this.props.data,
							onChange: this.props.onChange,
							inputAddon: 'm',
							inputAddonType: 'square'
						}),
						React.createElement(
							'div',
							{ className: 'form-group' },
							React.createElement(
								'label',
								null,
								'Jag bedömmer att mitt bohag är:'
							),
							React.createElement('br', null),
							React.createElement(Radio, {
								label: 'Normalt',
								idName: 'bohag',
								error: this.props.errors.bohag,
								className: 'radio-inline',
								onChange: this.props.onChange,
								data: this.props.data,
								identifier: '1',
								value: 'normalt' }),
							React.createElement(Radio, {
								label: 'Sparsamt',
								idName: 'bohag',
								error: this.props.errors.bohag,
								className: 'radio-inline',
								onChange: this.props.onChange,
								data: this.props.data,
								identifier: '2',
								value: 'sparsamt' }),
							React.createElement(Radio, {
								label: 'Välmöblerat',
								idName: 'bohag',
								error: this.props.errors.bohag,
								className: 'radio-inline',
								onChange: this.props.onChange,
								data: this.props.data,
								identifier: '3',
								value: 'valmoblerat' })
						)
					),
					React.createElement(
						'div',
						{ className: 'col-sm-4' },
						React.createElement(Input, {
							type: 'text',
							labelName: 'Övrig information',
							idName: 'otherInfo',
							error: this.props.errors.otherInfo,
							placeholder: 'Övrig information',
							desc: 'Ange övrig information som kan tänkas vara bra att veta.',
							data: this.props.data,
							onChange: this.props.onChange
						}),
						React.createElement(
							'div',
							{ className: 'form-group' },
							React.createElement(
								'label',
								null,
								'Önskas flyttstädning?'
							),
							React.createElement('br', null),
							React.createElement(Radio, {
								label: 'Nej',
								idName: 'movingHelp',
								error: this.props.errors.movingHelp,
								className: 'radio-inline',
								onChange: this.props.onChange,
								data: this.props.data,
								identifier: '1',
								value: '0' }),
							React.createElement(Radio, {
								label: 'Ja',
								idName: 'movingHelp',
								error: this.props.errors.movingHelp,
								className: 'radio-inline',
								onChange: this.props.onChange,
								data: this.props.data,
								identifier: '2',
								value: '1' })
						),
						React.createElement(
							'div',
							{ className: 'form-group' },
							React.createElement(
								'label',
								null,
								'Önskas packningshjälp?'
							),
							React.createElement('br', null),
							React.createElement(Radio, {
								label: 'Nej',
								idName: 'packingHelp',
								error: this.props.errors.packingHelp,
								className: 'radio-inline',
								onChange: this.props.onChange,
								data: this.props.data,
								identifier: '1',
								value: '0' }),
							React.createElement(Radio, {
								label: 'Ja',
								idName: 'packingHelp',
								error: this.props.errors.packingHelp,
								className: 'radio-inline',
								onChange: this.props.onChange,
								data: this.props.data,
								identifier: '2',
								value: '1' })
						),
						React.createElement(Checkbox, {
							label: 'Jag vill ha magasinering',
							idName: 'useMagazine',
							value: 'magazine',
							className: 'checkbox-inline',
							onChange: this.props.onChange,
							data: this.props.data,
							'default': false
						})
					),
					React.createElement(
						'div',
						{ className: 'col-sm-4' },
						packingHelpElement
					),
				)
			),
			newAddressFields,
			React.createElement(
				'div',
				{ className: 'col-sm-12', style: { border: '1px solid #CCC', backgroundColor: '#FFF', marginBottom: '20px', padding: '15px' } },
				React.createElement(
					'div',
					{ className: 'row' },
					React.createElement(
						'div',
						{ className: 'col-sm-12' },
						React.createElement(Checkbox, {
							label: 'Jag godkänner lagring av personuppgifter',
							idName: 'agreement',
							error: this.props.errors.agreement,
							value: 'agreement',
							className: 'checkbox-inline',
							onChange: this.props.onChange,
							data: this.props.data,
							'default': false
						}),
					)
				)
			),
			React.createElement(
				'div',
				{ id: 'calculatedPrice', className: 'col-sm-12 hidden', style: { border: '1px solid #CCC', backgroundColor: '#FFF', marginBottom: '20px', padding: '15px' } },
				React.createElement(
					'div',
					{ className: 'row' },
					React.createElement(
						'div',
						{ className: 'col-sm-8 priceContainer', style: { fontSize: '22px' } },
						React.createElement(
							'big',
							null,
							React.createElement('span', { className: 'totalPriceTag' })
						),
						React.createElement('br', null),
						React.createElement('span', { className: 'cleaningPriceTag' }),
						React.createElement('br', null),
						React.createElement('span', { className: 'packingPriceTag' }),
						React.createElement('br', null),
						React.createElement('span', { className: 'magazinePriceTag' }),
						React.createElement('br', null),
						React.createElement('br', null),
						React.createElement('p', { id: 'messageForPrice' }),
					),
					React.createElement(
						'div',
						{ className: 'col-sm-4 text-right' },
						React.createElement('input', { id: 'submitBtn', type: 'button', value: 'Skicka förfrågan', className: 'btn-primary btn-big btn-move hidden', onClick: this.props.onSave })
					)
				)
			)
		);
	}
});


/* Main app, it all starts here */
var App = React.createClass({
	displayName: 'App',

	getInitialState: function getInitialState() {
		return {
			activeErrorHandler: false,
			client: false,
			data: {
				'customerPostal': '',
				'customerName': '',
				'customerAddress': '',
				'customerCity': '',
				'respondTo': '',
				'customerPhone': '',
				'customerEmail': '',
				'customerEmailCompare': '',
				'livingType': '',
				'size': '',
				'level': '',
				'elevator': '',
				'movingDate': '',
				'boyta': '',
				'biyta': '',
				'bohag': '',
				'movingHelp': '',
				'packingHelp': '',
				'packingHelpPacking': '',
				'packingHelpUnpacking': '',
				'packingHelpPackingPorcelain': '',
				'packingHelpUnpackingPorcelain': '',
				'packingHelpNeedBoxes': '',
				'packingHelpBoxes': '',
				'otherInfo': '',
				'newCustomerPostal': '',
				'newCustomerAddress': '',
				'newCustomerCity': '',
				'newLivingType': '',
				'newLevel': '',
				'newElevator': '',
				'agreement': ''
			},
			errors: {
				'customerPostal': '',
				'customerName': '',
				'customerAddress': '',
				'customerCity': '',
				'respondTo': '',
				'customerPhone': '',
				'customerEmail': '',
				'customerEmailCompare': '',
				'livingType': '',
				'size': '',
				'level': '',
				'elevator': '',
				'movingDate': '',
				'boyta': '',
				'biyta': '',
				'bohag': '',
				'movingHelp': '',
				'packingHelp': '',
				'packingHelpPacking': '',
				'packingHelpUnpacking': '',
				'packingHelpPackingPorcelain': '',
				'packingHelpUnpackingPorcelain': '',
				'packingHelpNeedBoxes': '',
				'packingHelpBoxes': '',
				'otherInfo': '',
				'newCustomerPostal': '',
				'newCustomerAddress': '',
				'newCustomerCity': '',
				'newLivingType': '',
				'newLevel': '',
				'newElevator': '',
				'agreement': ''
			}
		};
	},
	componentDidMount: function componentDidMount() {
		this.setState({ client: true });
		this.calcHandler();
	},
	setDataState: function setDataState(event) {
		var field = event.target.name;
		var value = event.target.value;
		switch (event.target.type) {
			case 'checkbox':
				this.state.data[field] = event.target.checked;
				break;
			default:
				this.state.data[field] = value;
		}
		this.setState({ data: this.state.data });
		if (field === 'livingType' || field === 'size' || field === 'useMagazine' || field === 'packingHelp' || field === 'movingHelp' || field === 'boyta') {
			this.calcHandler();
		}
		// if (this.state.activeErrorHandler) { this.isFormValid(this.state.data); }
	},
	calcPrice: function calcPrice(data) {
		var size = data.size.length === 1 ? parseInt(data.size) : data.size;
		var livingType = data.livingType;
		var useMagazine = data.useMagazine;
		var packingHelp = data.packingHelp == '1' ? true : false;
		var perMagazineCost = 745;
		var price = 0;
		var perMonthPrice = 0;
		var message = 'Ungefärligt pris är efter <a href="https://www.skatteverket.se/privat/fastigheterochbostad/rotochrutarbete/rutarbeten.106.5c1163881590be297b53de7.html" target="_blank" title="Läs mer om rutarbeten i ny flik">RUT-avdrag (ca 40%)</a> och beräknat inom 036-området. För en mer exakt offert, skicka förfrågan till oss.';
		if (livingType === 'villa' || livingType === 'radhus') {
			price = 9000;
		} else {
			switch (size) {
				case 1:
					price = 3000;
					break;
				case 2:
					price = 4500;
					break;
				case 3:
					price = 5500;
					break;
				case 4:
					price = 7000;
					break;
				case 5:
					price = 8000;
					break;
				case '6+':
					price = 'unknown';
					break;
			}
		}
		if (useMagazine && size !== '6+') {
			perMonthPrice = perMagazineCost * size;
		}

		return [price, perMonthPrice, message, livingType];
	},












	calcHandler: function calcHandler() {
		var data = this.state.data;
		this.state.activeErrorHandler = true;
		// if (!this.isFormValid(data)) {return;}

		var result = {
			calculatedPrice: this.calcPrice(data),
			cleaningPrice: (data.movingHelp == 1)? 45 * Math.max(data.boyta, 45): 0, //45kr * kvm min 45 //movingHelp  = cleaning. wtf?
			packingPrice: (data.packingHelp == 1)? 1000 + data.size * 500 : 0 //1000kr bas + room * 500   
		};
		
		if(data.size.includes('+')){
			result.cleaningPrice = '+'
			result.packingPrice = '+'
		}
		

		var calculatedPriceContainer = $('#calculatedPrice');
		var submitBtn = $('#submitBtn');
		var messageContainer = $('#messageForPrice');
		

		if (result.calculatedPrice[0] === 0) {
			calculatedPriceContainer.addClass('hidden');
			submitBtn.addClass('hidden');
		} else {
			calculatedPriceContainer.removeClass('hidden');
			if (result.calculatedPrice[0] === 'unknown') {
				calculatedPriceContainer.find('.totalPriceTag').text('Ett ungefärligt pris kunde inte räknas ut.');
				calculatedPriceContainer.find('.magazinePriceTag').text('Skicka en förfrågan om offert för att få prisförslag.');
			
				calculatedPriceContainer.find('.cleaningPriceTag').text('')
				calculatedPriceContainer.find('.packingPriceTag').text('')
			} else {
				calculatedPriceContainer.find('.totalPriceTag').text('Ungefärligt pris: ' + result.calculatedPrice[0] + 'kr * ');
				if (result.calculatedPrice[1] !== 0) {
					if (result.calculatedPrice[3] === 'villa' || result.calculatedPrice[3] === 'radhus') {
						calculatedPriceContainer.find('.magazinePriceTag').text('Skicka förfrågan för att få mer exakt offert på magasinering med dessa inställningar.');
					} else {
						calculatedPriceContainer.find('.magazinePriceTag').text('Magasinering: ca ' + result.calculatedPrice[1] + 'kr/mån *');
					}
				} else {
					calculatedPriceContainer.find('.magazinePriceTag').text('');
				}

				if(result.cleaningPrice > 0 || result.cleaningPrice == '+') {
					if (result.cleaningPrice == '+') {
						calculatedPriceContainer.find('.cleaningPriceTag').text('Skicka förfrågan för att få mer exakt offert på flyttstäd med dessa inställningar.')
					} else {
						calculatedPriceContainer.find('.cleaningPriceTag').text('Flyttstäd ca: ' + result.cleaningPrice + 'kr *')
					}
					
				} else {
					calculatedPriceContainer.find('.cleaningPriceTag').text('')
				}

				if(result.packingPrice > 0  || result.packingPrice == '+') { 
					if (result.packingPrice == '+') {
						calculatedPriceContainer.find('.packingPriceTag').text('Skicka förfrågan för att få mer exakt offert på packningshjälp med dessa inställningar.')
					}else {
						calculatedPriceContainer.find('.packingPriceTag').text('Packningshjälp ca: ' + result.packingPrice + 'kr *')
					}
					
				} else {
					calculatedPriceContainer.find('.packingPriceTag').text('')
				}
			}
			messageContainer.html('* ' + result.calculatedPrice[2]);
			submitBtn.removeClass('hidden');
		}

		return [result];
	},

	isFormValid: function isFormValid(data) {
		var formIsValid = true;
		this.state.errors = {}; // clear previous errors

		if (data.customerPostal.length <= 0) {
			this.state.errors.customerPostal = 'Du måste fylla i ditt postnummer.';
			formIsValid = false;
		}
		if (data.customerName.length <= 0) {
			this.state.errors.customerName = 'Du måste fylla i ditt namn.';
			formIsValid = false;
		}
		if (data.customerAddress.length <= 0) {
			this.state.errors.customerAddress = 'Du måste fylla i adress.';
			formIsValid = false;
		}
		if (data.customerCity.length <= 0) {
			this.state.errors.customerCity = 'Du måste fylla i stad.';
			formIsValid = false;
		}
		if (data.customerPhone.length <= 4) {
			this.state.errors.customerPhone = 'Numret angivet är för kort';
			formIsValid = false;
		}
		if (data.customerEmail.length <= 0) {
			this.state.errors.customerEmail = 'Du måste fylla i din epost.';
			formIsValid = false;
		}
		if (data.customerEmailCompare !== data.customerEmail) {
			this.state.errors.customerEmailCompare = 'Eposten i detta fältet måste matcha epost-adressen i fältet ovan.';
			formIsValid = false;
		}
		if (data.movingDate.length <= 0) {
			this.state.errors.movingDate = 'Du måste välja ett datum';
			formIsValid = false;
		}
		if (data.boyta <= 0 || data.boyta === '') {
			this.state.errors.boyta = 'Fyll i hur stor boyta som ska städas';
			formIsValid = false;
		}
		if (!data.agreement || data.agreement === '') {
			this.state.errors.agreement = 'Du måste godkänna lagring av personuppgifter för att gå vidare';
			formIsValid = false;
		}
		/*if (data.biyta <= 0 || data.biyta === '') {
			this.state.errors.biyta = 'Fyll i hur stor boyta som ska städas';
			formIsValid = false;
		}*/
		if (data.packingHelp === '1' && data.packingHelpNeedBoxes === '1') {
			if (data.packingHelpBoxes <= 0 || data.packingHelpBoxes === '') {
				this.state.errors.packingHelpBoxes = 'Fyll i antal kargonger som behövs';
				formIsValid = false;
			}
		}

		/*if (data.newCustomerAddress.length <= 0){
  	this.state.errors.newCustomerAddress = 'Du måste fylla i adress.';
  	formIsValid = false;
  }
  if (data.newCustomerCity.length <= 0){
  	this.state.errors.newCustomerCity = 'Du måste fylla i stad.';
  	formIsValid = false;
  }*/

		this.setState({ errors: this.state.errors });
		return formIsValid;
	},
	sendMail: function sendMail(event) {
		var result = this.calcHandler();
		// console.log(result);

		if (this.isFormValid(this.state.data)) {
			// var state = this.state.data;
			// var action = 'send_mail_from_calc';
			var data = this.state.data;
			$.ajax({
				type: 'POST',
				dataType: 'text',
				data: data
			}).done(function (data) {
				$('#sendRequestComplete').modal();
			});
		} else {
			event.preventDefault();
			return;
		}
	},
	render: function render() {
		return React.createElement(
			'div',
			null,
			React.createElement(Form, {
				data: this.state.data,
				onChange: this.setDataState,
				onCalc: this.calcHandler,
				onSave: this.sendMail,
				method: 'POST',
				errors: this.state.errors
			})
		);
	}
});

React.render(React.createElement(App, null), document.getElementById('calcApp'));

$(function () {
	$('[data-toggle="popover"]').popover();
});
/*<div className="col-sm-12 text-right">
<input id="calcBtn" type="button" value="Räkna ut ditt pris" className="btn-primary btn-big btn-move" onClick={this.props.onCalc} />
</div>*/