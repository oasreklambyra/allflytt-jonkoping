$ = jQuery;

var Input = React.createClass({
	
	render : function() 
	{
		var label = null;
		if (this.props.labelName && this.props.labelName.length > 0) 
		{
			label = (<label htmlFor={this.props.idName}>{this.props.labelName}</label>);
		}
		
		var inputAddon = null;
		if (this.props.inputAddon && this.props.inputAddon.length > 0) 
		{
			var type =  this.props.inputAddonType === 'square' ? (<sup>2</sup>) : '';
			inputAddon = (<div className="input-group-addon">{this.props.inputAddon}{type}</div>);
		}

		var desc = null;
		if (this.props.desc && this.props.desc.length > 0)
		{
			desc = (
				<div className="input-group-addon" style={{padding: '0'}}>
					<div data-toggle="popover" data-placement="top" data-content={this.props.desc} style={{cursor: 'pointer', padding: '6px 12px'}}>
						<i className="fa fa-info"></i>
					</div>
				</div>
			);
		}

		var wrapperClass = 'form-group';
		if (this.props.error && this.props.error.length > 0) {wrapperClass += ' ' + 'has-error';}

		return (
			<div className={wrapperClass}>
				{label}
				<div className='input-group' style={{width: '100%'}}>
					{inputAddon}
					<input id={this.props.idName}
						className='form-control'
						type={this.props.type} 
						value={this.props.value} 
						onChange={this.props.onChange} 
						name={this.props.idName} 
						placeholder={this.props.placeholder} 
						/>
					{desc}
				</div>
				<div className="text-danger"><strong>{this.props.error}</strong></div>
			</div>
		);
	}
});

var Checkbox = React.createClass({
	componentWillMount : function() {
		this.props.data[this.props.idName] = this.props.default;
	},
	render : function() {
		return (
			<label className={this.props.className} >
				<input id={this.props.idName} 
					type='checkbox'
					name={this.props.idName} 
					onChange={this.props.onChange}
					value={this.props.value} 
					/>
				<span>{this.props.label}</span>
			</label>
		);
	}
});

var Select = React.createClass({
	currentOption : 0,
	componentWillMount : function() {
		this.props.data[this.props.idName] = this.props.default;
	},
	shouldComponentUpdate : function() {
		if (this.currentOption === this.props.data[this.props.idName]) { return false;}
		else { return true;}
	},
	render : function() {
		var options = [];
		this.currentOption = this.props.data[this.props.idName] ? this.props.data[this.props.idName] : this.props.default;
		this.props.options.forEach(function(option) {
			options.push(
				<option value={option.value} key={option.value}>{option.label}</option>
			);
		});
		return (
			<div className="form-group">
				<label htmlFor={this.props.idName}>{this.props.labelName}</label>
				<select className="form-control" 
					id={this.props.idName} name={this.props.idName} 
					onChange={this.props.onChange}
					value={this.currentOption} 
					>
					{options}
				</select>
			</div>
		);
	}
});

var Radio = React.createClass({
	currentChecked : '1',
	componentWillMount : function() {
		if (this.props.identifier === '1')
		{
			this.props.data[this.props.idName] = this.props.value;
		}
	},
	shouldComponentUpdate : function() {
		if (this.currentChecked === this.props.data[this.props.idName]) {return false;}
		else {return true;}
	},
	render : function() {
		this.currentChecked = this.props.data[this.props.idName] === this.props.value ? this.props.identifier : false;
		var identifier = this.props.idName+this.props.identifier;
		return (
			<label className={this.props.className}>
				<input 
					id={identifier} 
					type="radio" 
					checked={this.currentChecked === this.props.identifier} 
					name={this.props.idName} 
					onChange={this.props.onChange} 
					value={this.props.value} 
					/>
				<span>{this.props.label}</span>
			</label>
		);
	}
});

/* Contains the Form */
var Form = React.createClass({
	showPackingHelp : false,
	showNewAddressFields : true,
	shouldComponentUpdate : function() {
		if (this.props.data.packingHelp === "1")
		{
			this.showPackingHelp = true;
		}
		else 
		{
			this.showPackingHelp = false;
		}
		
		if (this.props.data.useMagazine === true)
		{
			this.showNewAddressFields = false;
		}
		else 
		{
			this.showNewAddressFields = true;
		}
		return true;

	},
	getNewAddressFields : function () {
		return ( 
			<div className="col-sm-12" style={{border: '1px solid #CCC', backgroundColor: '#FFF', marginBottom: '20px', padding: '15px'}}>
				<h2 style={{textTransform: 'uppercase', marginBottom: '20px', marginTop: '30px'}}>Nya boendeuppgifter</h2>

				<div className="row">
					<div className="col-sm-4">
						<Input 
							type="text"
							labelName="Adress"
							idName="newCustomerAddress"
							error={this.props.errors.newCustomerAddress}
							placeholder="Adress."
							desc="Ange den nya adressen."
							data={this.props.data}
							onChange={this.props.onChange}
							/>
						<Input 
							type="text"
							labelName="Ort"
							idName="newCustomerCity"
							error={this.props.errors.newCustomerCity}
							placeholder="Stad"
							desc="Ange staden som den nya adressen ligger i."
							data={this.props.data}
							onChange={this.props.onChange}
							/>
					</div>
					<div className="col-sm-4">
						<Select
							labelName="Bostadstyp"
							onChange={this.props.onChange}
							data={this.props.data}
							idName="newLivingType"
							error={this.props.errors.newLivingType}
							default="laganhet"
							options={[
								{
									value : 'laganhet',
									label : 'Lägenhet'
								}, 
								{
									value : 'radhus',
									label : 'Radhus'
								}, 
								{
									value : 'villa',
									label : 'Villa'
								}, 
								{
									value : 'kontor',
									label : 'Kontor'
								}, 
								{
									value : 'magasin',
									label : 'Magasin'
								}, 
								{
									value : 'annat',
									label : 'Annat'
								}
							]} />

						<Select
							labelName="Våning"
							onChange={this.props.onChange}
							data={this.props.data}
							idName="newLevel"
							error={this.props.errors.newLevel}
							default="0"
							options={[
								{
									value : '0',
									label : '0'
								},
								{
									value : '1',
									label : '1'
								}, 
								{
									value : '2',
									label : '2'
								}, 
								{
									value : '3',
									label : '3'
								}, 
								{
									value : '4',
									label : '4'
								}, 
								{
									value : '5',
									label : '5'
								}, 
								{
									value : '6',
									label : '6'
								},
								{
									value : '7',
									label : '7'
								},
								{
									value : 'moreThan7',
									label : 'Fler än 7'
								}
							]} />
					</div>
					<div className="col-sm-4">
						<Select
							labelName="Hiss"
							onChange={this.props.onChange}
							data={this.props.data}
							idName="newElevator"
							error={this.props.errors.newElevator}
							default={false}
							options={[
								{
									value : false,
									label : 'Nej'
								},
								{
									value : 'small',
									label : 'Liten hiss'
								}, 
								{
									value : 'large',
									label : 'Stor hiss (4 pers eller fler)'
								}, 
								{
									value : 'unknown',
									label : 'Vet ej'
								}
							]} />
					</div>
				</div>

			</div>
		);
	},
	getPackingHelpElements : function() {
		return (
			<div>
				<div className="form-group">
					<label>Vilken typ av packningshjälp?</label><br />
					<Checkbox 
						label="Packning"
						idName="packingHelpPacking"
						error={this.props.errors.packingHelpPacking}
						value="packing" 
						className="checkbox-inline"
						onChange={this.props.onChange} 
						data={this.props.data}
						default={false} />
					<Checkbox 
						label="Uppackning"
						idName="packingHelpUnpacking"
						error={this.props.errors.packingHelpUnpacking}
						value="unpacking"
						className="checkbox-inline"
						onChange={this.props.onChange}
						data={this.props.data}
						default={false} />
				</div>
				<div className="form-group">
					<label>Endast porslin?</label><br />
					<Checkbox 
						label="Packning"
						idName="packingHelpPackingPorcelain"
						error={this.props.errors.packingHelpPackingPorcelain}
						value="packing" 
						className="checkbox-inline"
						onChange={this.props.onChange} 
						data={this.props.data}
						default={false} />
					<Checkbox 
						label="Uppackning"
						idName="packingHelpUnpackingPorcelain"
						error={this.props.errors.packingHelpUnpackingPorcelain}
						value="unpacking"
						className="checkbox-inline"
						onChange={this.props.onChange}
						data={this.props.data}
						default={false} />
				</div>

				<div className="form-group">
					<label>Lån av flyttkargonger?</label><br />
					<Radio 
						label="Nej"
						idName="packingHelpNeedBoxes"
						error={this.props.errors.packingHelpNeedBoxes}
						className="radio-inline"
						onChange={this.props.onChange} 
						data={this.props.data}
						identifier="1"
						value="0" />
					<Radio 
						label="Ja"
						idName="packingHelpNeedBoxes"
						error={this.props.errors.packingHelpNeedBoxes}
						className="radio-inline"
						onChange={this.props.onChange} 
						data={this.props.data}
						identifier="2"
						value="1" />
				</div>
				<Input 
					type="number"
					labelName="Hur många kartonger?"
					idName="packingHelpBoxes"
					error={this.props.errors.packingHelpBoxes}
					placeholder="Antal kartonger"
					desc="Ange antalet flyttkargoner som behöver lånas."
					data={this.props.data}
					onChange={this.props.onChange}
					/>
			</div>
		);
	},
	render : function() 
	{
		var packingHelpElement = '';
		if (this.showPackingHelp)
		{
			packingHelpElement = this.getPackingHelpElements();
		}

		var newAddressFields = '';
		if (this.showNewAddressFields)
		{
			newAddressFields = this.getNewAddressFields();
		}

		return (
			<form method={this.props.method}>
				<div className="col-sm-12" style={{border: '1px solid #CCC', backgroundColor: '#FFF', marginBottom: '20px', padding: '15px'}}>
					<h2 style={{textTransform: 'uppercase', marginBottom: '20px', marginTop: '30px'}}>Boendeuppgifter</h2>
					<div className="row">
						<div className="col-sm-4">
							<Input 
								type="text"
								labelName="Namn"
								idName="customerName"
								error={this.props.errors.customerName}
								placeholder="Namn"
								desc="Ange ditt för- och efternamn."
								data={this.props.data}
								onChange={this.props.onChange}
								/>
							<Input 
								type="text"
								labelName="Adress"
								idName="customerAddress"
								error={this.props.errors.customerAddress}
								placeholder="Adress"
								desc="Ange adressen som städningen ska utföras i."
								data={this.props.data}
								onChange={this.props.onChange}
								/>
							<Input 
								type="text"
								labelName="Ort"
								idName="customerCity"
								error={this.props.errors.customerCity}
								placeholder="Stad"
								desc="Ange staden som städningen ska utföras i."
								data={this.props.data}
								onChange={this.props.onChange}
								/>
							
							<Select
								labelName="Vill ha svar via"
								onChange={this.props.onChange}
								data={this.props.data}
								idName="respondTo"
								error={this.props.errors.respondTo}
								default='email'
								options={[
									{
										value : 'email',
										label : 'E-post'
									},
									{
										value : 'phone',
										label : 'Telefon'
									}, 
									{
										value : 'both',
										label : 'E-post \& Telefon'
									}
								]} />
						</div>
						<div className="col-sm-4">
							<Input 
								type="text"
								labelName="Telefon"
								idName="customerPhone"
								error={this.props.errors.customerPhone}
								placeholder="Telefonnummer"
								desc="Ange det telefonnummer som en kontaktperson kan nås på."
								data={this.props.data}
								onChange={this.props.onChange}
								/>

							<Input 
								type="email"
								labelName="E-post"
								idName="customerEmail"
								error={this.props.errors.customerEmail}
								placeholder="E-post"
								desc="Ange e-postadressen som en kontaktperson kan nås på."
								data={this.props.data}
								onChange={this.props.onChange}
								/>

							<Input 
								type="email"
								labelName="Bekräfta e-post"
								idName="customerEmailCompare"
								error={this.props.errors.customerEmailCompare}
								placeholder="Bekräfta e-post"
								desc="Ange e-postadressen igen som en kontaktperson kan nås på för att vara säker på att rätt adress har angivits."
								data={this.props.data}
								onChange={this.props.onChange}
								/>

							<Select
								labelName="Bostadstyp"
								onChange={this.props.onChange}
								data={this.props.data}
								idName="livingType"
								error={this.props.errors.livingType}
								default="laganhet"
								options={[
									{
										value : 'laganhet',
										label : 'Lägenhet'
									}, 
									{
										value : 'radhus',
										label : 'Radhus'
									}, 
									{
										value : 'villa',
										label : 'Villa'
									}, 
									{
										value : 'kontor',
										label : 'Kontor'
									}, 
									{
										value : 'magasin',
										label : 'Magasin'
									}, 
									{
										value : 'annat',
										label : 'Annat'
									}
								]} />
						</div>
						<div className="col-sm-4">
							<Select
								labelName="Storlek"
								onChange={this.props.onChange}
								data={this.props.data}
								idName="size"
								error={this.props.errors.size}
								default="1"
								options={[
									{
										value : '1',
										label : '1 rum och kök'
									}, 
									{
										value : '2',
										label : '2 rum och kök'
									}, 
									{
										value : '3',
										label : '3 rum och kök'
									}, 
									{
										value : '4',
										label : '4 rum och kök'
									}, 
									{
										value : '5',
										label : '5 rum och kök'
									},
									{
										value : '6+',
										label : '6 eller fler rum'
									}
								]} />

							<Select
								labelName="Våning"
								onChange={this.props.onChange}
								data={this.props.data}
								idName="level"
								error={this.props.errors.level}
								default="0"
								options={[
									{
										value : '0',
										label : '0'
									},
									{
										value : '1',
										label : '1'
									}, 
									{
										value : '2',
										label : '2'
									}, 
									{
										value : '3',
										label : '3'
									}, 
									{
										value : '4',
										label : '4'
									}, 
									{
										value : '5',
										label : '5'
									}, 
									{
										value : '6',
										label : '6'
									},
									{
										value : '7',
										label : '7'
									},
									{
										value : 'moreThan7',
										label : 'Fler än 7'
									}
								]} />

							<Select
								labelName="Hiss"
								onChange={this.props.onChange}
								data={this.props.data}
								idName="elevator"
								error={this.props.errors.elevator}
								default={false}
								options={[
									{
										value : false,
										label : 'Nej'
									},
									{
										value : 'small',
										label : 'Liten hiss'
									}, 
									{
										value : 'large',
										label : 'Stor hiss (4 pers eller fler)'
									}, 
									{
										value : 'unknown',
										label : 'Vet ej'
									}
								]} />
						</div>
					</div>
				</div>

				<div className="col-sm-12" style={{border: '1px solid #CCC', backgroundColor: '#FFF', marginBottom: '20px', padding: '15px'}}>
					<h2 style={{textTransform: 'uppercase', marginBottom: '20px', marginTop: '30px'}}>Flyttuppgifter</h2>
					<div className="row">
						<div className="col-sm-4">
							<Input 
								type="text"
								labelName="Flyttdatum"
								idName="movingDate"
								error={this.props.errors.movingDate}
								placeholder="T.ex: 2015-09-25"
								desc="Ange datum för flytt, följ mönstret år-månad-dag."
								data={this.props.data}
								onChange={this.props.onChange}
								/>
							<Input 
								type="number"
								labelName="Boyta"
								idName="boyta"
								error={this.props.errors.boyta}
								placeholder="Ange boyta"
								desc="Ange kvadratmeter för den totala boytan."
								data={this.props.data}
								onChange={this.props.onChange}
								inputAddon="m"
								inputAddonType="square"
								/>
							<Input 
								type="number"
								labelName="Biyta"
								idName="biyta"
								error={this.props.errors.biyta}
								placeholder="(källare, förråd)"
								desc="Ange kvadratmeter för totala biytan, så som källare och/eller förråd."
								data={this.props.data}
								onChange={this.props.onChange}
								inputAddon="m"
								inputAddonType="square"
								/>

							<div className="form-group">
								<label>Jag bedömmer att mitt bohag är:</label><br />
								<Radio 
									label="Normalt"
									idName="bohag"
									error={this.props.errors.bohag}
									className="radio-inline"
									onChange={this.props.onChange} 
									data={this.props.data}
									identifier="1"
									value="normalt" />
								<Radio 
									label="Sparsamt"
									idName="bohag"
									error={this.props.errors.bohag}
									className="radio-inline"
									onChange={this.props.onChange} 
									data={this.props.data}
									identifier="2"
									value="sparsamt" />
								<Radio 
									label="Välmöblerat"
									idName="bohag"
									error={this.props.errors.bohag}
									className="radio-inline"
									onChange={this.props.onChange} 
									data={this.props.data}
									identifier="3"
									value="valmoblerat" />
							</div>
							
						</div>
						<div className="col-sm-4">
							<Input 
								type="text"
								labelName="Övrig information"
								idName="otherInfo"
								error={this.props.errors.otherInfo}
								placeholder="Övrig information"
								desc="Ange övrig information som kan tänkas vara bra att veta."
								data={this.props.data}
								onChange={this.props.onChange}
								/>

							<div className="form-group">
								<label>Önskas flyttstädning?</label><br />
								<Radio 
									label="Nej"
									idName="movingHelp"
									error={this.props.errors.movingHelp}
									className="radio-inline"
									onChange={this.props.onChange} 
									data={this.props.data}
									identifier="1"
									value="0" />
								<Radio 
									label="Ja"
									idName="movingHelp"
									error={this.props.errors.movingHelp}
									className="radio-inline"
									onChange={this.props.onChange} 
									data={this.props.data}
									identifier="2"
									value="1" />
							</div>

							<div className="form-group">
								<label>Önskas packningshjälp?</label><br />
								<Radio 
									label="Nej"
									idName="packingHelp"
									error={this.props.errors.packingHelp}
									className="radio-inline"
									onChange={this.props.onChange} 
									data={this.props.data}
									identifier="1"
									value="0" />
								<Radio 
									label="Ja"
									idName="packingHelp"
									error={this.props.errors.packingHelp}
									className="radio-inline"
									onChange={this.props.onChange} 
									data={this.props.data}
									identifier="2"
									value="1" />
							</div>

							<Checkbox 
								label="Jag vill ha magasinering"
								idName="useMagazine"
								value="magazine" 
								className="checkbox-inline"
								onChange={this.props.onChange} 
								data={this.props.data}
								default={false}
								/>
						</div>
						<div className="col-sm-4">
							{packingHelpElement}
						</div>
					</div>
				</div>
			
				{newAddressFields}

				{/*<div className="col-sm-12 text-right">
					<input id="calcBtn" type="button" value="Räkna ut ditt pris" className="btn-primary btn-big btn-move" onClick={this.props.onCalc} />
				</div>*/}
				<div id="calculatedPrice" className="col-sm-12 hidden" style={{border: '1px solid #CCC', backgroundColor: '#FFF', marginBottom: '20px', padding: '15px'}}>
					<div className="row">
						<div className="col-sm-8 priceContainer" style={{fontSize: '22px'}}>
							<big><span className="totalPriceTag"></span></big><br />
							<span className="magazinePriceTag"></span>
							<p id="messageForPrice"></p>

						</div>
						<div className="col-sm-4 text-right">
							<input id="submitBtn" type="button" value="Skicka förfrågan" className="btn-primary btn-big btn-move hidden" onClick={this.props.onSave} />
						</div>
					</div>
				</div>
			</form>
		);
	}
});
/* Main app, it all starts here */
var App = React.createClass({
	getInitialState : function() 
	{
		return {
			activeErrorHandler : false,
			client: false,
			data : {
				'customerName' : '',
				'customerAddress' : '',
				'customerCity' : '',
				'respondTo' : '',
				'customerPhone' : '',
				'customerEmail' : '',
				'customerEmailCompare' : '',
				'livingType' : '',
				'size' : '',
				'level' : '',
				'elevator' : '',
				'movingDate' : '',
				'boyta' : '',
				'biyta' : '',
				'bohag' : '',
				'movingHelp' : '',
				'packingHelp' : '',
				'packingHelpPacking' : '',
				'packingHelpUnpacking' : '',
				'packingHelpPackingPorcelain' : '',
				'packingHelpUnpackingPorcelain' : '',
				'packingHelpNeedBoxes' : '',
				'packingHelpBoxes' : '',
				'otherInfo' : '',
				'newCustomerAddress' : '',
				'newCustomerCity' : '',
				'newLivingType' : '',
				'newLevel' : '',
				'newElevator' : ''
			},
			errors : {
				'customerName' : '',
				'customerAddress' : '',
				'customerCity' : '',
				'respondTo' : '',
				'customerPhone' : '',
				'customerEmail' : '',
				'customerEmailCompare' : '',
				'livingType' : '',
				'size' : '',
				'level' : '',
				'elevator' : '',
				'movingDate' : '',
				'boyta' : '',
				'biyta' : '',
				'bohag' : '',
				'movingHelp' : '',
				'packingHelp' : '',
				'packingHelpPacking' : '',
				'packingHelpUnpacking' : '',
				'packingHelpPackingPorcelain' : '',
				'packingHelpUnpackingPorcelain' : '',
				'packingHelpNeedBoxes' : '',
				'packingHelpBoxes' : '',
				'otherInfo' : '',
				'newCustomerAddress' : '',
				'newCustomerCity' : '',
				'newLivingType' : '',
				'newLevel' : '',
				'newElevator' : ''
			}
		};
	},
	componentDidMount : function() 
	{
		this.setState({client: true});
		this.calcHandler();
	},
	setDataState : function(event) 
	{
		var field = event.target.name;
		var value = event.target.value;
		switch (event.target.type)
		{
			case 'checkbox': 
				this.state.data[field] = event.target.checked;
				break;
			/*case 'select-one':
				this.state.data[field] = event.target.selected;
				break;*/
			default:
				this.state.data[field] = value;	
		}
		// console.log(this.state.data);
		this.setState({data: this.state.data});
		if (field === 'livingType' || field === 'size' || field === 'useMagazine')
		{
			this.calcHandler();
		}
		// if (this.state.activeErrorHandler) { this.isFormValid(this.state.data); }
	},
	calcPrice : function(data)
	{
		var size = data.size.length === 1 ? parseInt(data.size) : data.size;
		var livingType = data.livingType;
		var useMagazine = data.useMagazine;
		var perMagazineCost = 625;
		var price = 0;
		var perMonthPrice = 0;
		var message = 'För en mer exakt offert, skicka förfrågan till oss.';
		// console.log(size, livingType, useMagazine);
		if (livingType === 'villa' || livingType === 'radhus') 
		{
			price = 9000;
		}
		else 
		{
			switch (size)
			{
				case 1:
					price = 3000;
					break;
				case 2:
					price = 4500;
					break;
				case 3:
					price = 5500;
					break;
				case 4:
					price = 7000;
					break;
				case 5:
					price = 8000;
					break;
				case '6+':
					price = 'unknown';
					break;
			}
		}
		if (useMagazine && size !== '6+')
		{
			perMonthPrice = (perMagazineCost * size);
		}

		return [price, perMonthPrice, message, livingType];
	},
	calcHandler : function() 
	{
		var data = this.state.data;
		this.state.activeErrorHandler = true;
		// if (!this.isFormValid(data)) {return;}
		var result = {
			calculatedPrice : this.calcPrice(data)
		};
		// console.log(result);
		var calculatedPriceContainer = $('#calculatedPrice');
		var submitBtn = $('#submitBtn');
		var messageContainer = $('#messageForPrice');
		if (result.calculatedPrice[0] === 0) 
		{
			calculatedPriceContainer.addClass('hidden');
			submitBtn.addClass('hidden');
		}
		else 
		{
			calculatedPriceContainer.removeClass('hidden');
			if (result.calculatedPrice[0] === 'unknown')
			{
				calculatedPriceContainer.find('.totalPriceTag').text('Ett ungefärligt pris kunde inte räknas ut.');
				calculatedPriceContainer.find('.magazinePriceTag').text('Skicka en förfrågan om offert för att få prisförslag.');
			}
			else 
			{
				calculatedPriceContainer.find('.totalPriceTag').text('Ungefärligt pris: '+result.calculatedPrice[0]+'kr * ');
				if (result.calculatedPrice[1] !== 0) 
				{
					if (result.calculatedPrice[3] === 'villa' || result.calculatedPrice[3] === 'radhus')
					{
						calculatedPriceContainer.find('.magazinePriceTag').text('Skicka förfrågan för att få mer exakt offert på magasinering med dessa inställningar.');
					}
					else
					{
						calculatedPriceContainer.find('.magazinePriceTag').text('Magasinering: ca '+result.calculatedPrice[1]+'kr/mån *');
					}
				}
				else 
				{
					calculatedPriceContainer.find('.magazinePriceTag').text('');
					
				}
			}
			messageContainer.text('* '+result.calculatedPrice[2]);
			submitBtn.removeClass('hidden');
		}
		return result;
	},
	isFormValid : function(data) {
		var formIsValid = true;
		this.state.errors = {}; // clear previous errors

		if (data.customerName.length <= 0){
			this.state.errors.customerName = 'Du måste fylla i ditt namn.';
			formIsValid = false;
		}
		if (data.customerAddress.length <= 0){
			this.state.errors.customerAddress = 'Du måste fylla i adress.';
			formIsValid = false;
		}
		if (data.customerCity.length <= 0){
			this.state.errors.customerCity = 'Du måste fylla i stad.';
			formIsValid = false;
		}
		if (data.customerPhone.length <= 4){
			this.state.errors.customerPhone = 'Numret angivet är för kort';
			formIsValid = false;
		}
		if (data.customerEmail.length <= 0){
			this.state.errors.customerEmail = 'Du måste fylla i din epost.';
			formIsValid = false;
		}
		if (data.customerEmailCompare !== data.customerEmail){
			this.state.errors.customerEmailCompare = 'Eposten i detta fältet måste matcha epost-adressen i fältet ovan.';
			formIsValid = false;
		}
		if (data.movingDate.length <= 0){
			this.state.errors.movingDate = 'Du måste välja ett datum';
			formIsValid = false;
		}
		if (data.boyta <= 0 || data.boyta === ''){
			this.state.errors.boyta = 'Fyll i hur stor boyta som ska städas';
			formIsValid = false;
		}
		if (data.biyta <= 0 || data.biyta === ''){
			this.state.errors.biyta = 'Fyll i hur stor boyta som ska städas';
			formIsValid = false;
		}
		if (data.packingHelp === '1' && data.packingHelpNeedBoxes === '1') 
		{
			if (data.packingHelpBoxes <= 0 || data.packingHelpBoxes === ''){
				this.state.errors.packingHelpBoxes = 'Fyll i antal kargonger som behövs';
				formIsValid = false;
			}
		}

		/*if (data.newCustomerAddress.length <= 0){
			this.state.errors.newCustomerAddress = 'Du måste fylla i adress.';
			formIsValid = false;
		}
		if (data.newCustomerCity.length <= 0){
			this.state.errors.newCustomerCity = 'Du måste fylla i stad.';
			formIsValid = false;
		}*/

		this.setState({errors: this.state.errors});
		return formIsValid;
	},
	sendMail : function(event) 
	{
		var result = this.calcHandler();
		// console.log(result);
		
		if (this.isFormValid(this.state.data)) 
		{
			// var state = this.state.data;
			// var action = 'send_mail_from_calc';
			var data = this.state.data;
			$.ajax({
				type: 'POST',
				dataType: 'text',
				data: data,
			}).done(function(data) {
				$('#sendRequestComplete').modal();
			});
		}
		else
		{
			event.preventDefault();
			return;
		}
	},
	render : function()
	{
		return (
			<div>
				<Form 
					data={this.state.data}
					onChange={this.setDataState}
					onCalc={this.calcHandler}
					onSave={this.sendMail}
					method="POST"
					errors={this.state.errors}
					 />
			</div>
		);
	}
});

React.render(
	<App />,
	document.getElementById('calcApp')
);

$(function () {
  $('[data-toggle="popover"]').popover();
});

