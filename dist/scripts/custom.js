jQuery( document ).ready(function($) {
	var collapse = function() {
		if($( document ).width() < 768)
		{
			$('.navbar-nav').addClass('collapse');
		}
		else
		{
			$('.navbar-nav').removeClass('collapse');
		}
	}

	$('.fancybox').fancybox();
	collapse();

	$(window).resize(function(){
		collapse();
    })

	

});