<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

<div class="start" style="background-image:url('<?php echo $url; ?>');">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">
				<h1 class="white mtop">
					<?php the_title(); ?>
				</h1>

				<article class="ingress white">
					<?php the_content(); ?>
				</article>
			</div>

			<?php include('move.php'); ?>
		</div>
		
	</div>
</div>

<?php $url_2 = wp_get_attachment_url( get_field('bg_footer_top', 'options') ); ?>
<div style="background-image:url('<?php echo $url_2; ?>'); padding: 80px 0px; margin-bottom: 20px;">
	<div class="container">
	
		<div class="row">
			<div class="col-sm-8">
				<h2 class="h2-big white" style="margin-top: 29px; margin-bottom: 0px;">
					Vad säger våra kunder?
					
				</h2>
			</div>

			<div class="col-sm-4">
				<a class="btn-primary pull-right btn-big" style="display: inline; line-height: 1.4; padding: 26px 30px;" href="/referenser">Se referenser</a>
			</div>
		</div>
	</div>
</div>

<?php //if($_SERVER['REMOTE_ADDR'] == '146.66.236.58') : ?>
	<article class="video-block text-center">
		<div class="container">
			<div class="row">
				<div class="col-sm-offset-2 col-sm-8">
					<div class="video-ratio-16-9">
						<iframe class="video" width="420" height="315"
							src="https://www.youtube.com/embed/H69btCq9v6o"
							style="width: 600px;" webkitallowfullscreen mozallowfullscreen allowfullscreen>
						</iframe>
					</div>
				</div>
			</div>
		</div>
	</article>

	<style type="text/css">
		iframe.video{
			/*width: 600px;*/
			border: 0px;
		}
		.video-ratio-16-9 {
			position: relative;
			padding-bottom: 52.25%; /* 16:9 */
			padding-top: 25px;
			height: 0;
		}
		.video-ratio-16-9 iframe {
			position: absolute;
			top: 0;
			left: 0;
			width: 100% !important;
			height: 100% !important;
		}
	</style>
<?php //endif; ?>



<div class="container linkbox-wrapper">
	<?php 
	$args = array(
	'posts_per_page'   => -1,
	'orderby'          => 'menu_order',
	'order'            => 'ASC',
	'post_type'        => 'linkbox',
	'post_status'      => 'publish' ); 
	
	$posts = get_posts($args);
	$last = count($posts);
	$second_last = $last - 1;
	$cnt = 0;
	?>
	
	<div class="row row-top">
		<?php foreach($posts as $post) : setup_postdata($post); $cnt++; ?>

			<div class="col-sm-3 linkbox text-center <?php if($cnt == $last) : echo 'last'; elseif($cnt == $second_last) : echo 'second_last'; endif; ?>">
				<div style="border: 1px solid #ccc; background-color: white; min-height: 320px;">
					<h2 style="text-transform: uppercase; margin-bottom: 20px; margin-top: 30px;"><?php the_title(); ?></h2>

					<div style="padding-left: 15px; padding-right: 15px; margin-bottom: 5px;">
						<?php the_post_thumbnail('services-size', array('class' => 'img-responsive linkbox-img')); ?>
						<div style="margin-top: 15px;"><?php the_content(); ?></div>
					</div>
				</div>
				<div class="text-center linkbox-btn-wrapper">
					<a href="<?php the_field('page_link') ?>" class="btn-primary btn-service">
						Mer info
					</a>
				</div>
			</div>

			<?php if($cnt % 4 == 0) : ?>
				<div class="hidden-xs clearfix"></div>
			<?php endif; ?>
		
		<?php 
		endforeach;
		wp_reset_postdata(); 
		?>
	</div>
	
</div>

<div class="container" style="margin-bottom: 80px;">
	<div class="row">
		<div class="col-sm-12">
			<h2 class="h2-big text-center mtop fancy"><span class="fancy-span">Bra att veta</span></h2>

			<?php 
			$args = array(
			'posts_per_page'   => -1,
			'orderby'          => 'menu_order',
			'order'            => 'ASC',
			'post_type'        => 'good_to_know',
			'post_status'      => 'publish' ); 
			
			$posts = get_posts($args);
			$last = count($posts);
			$second_last = $last - 1;
			$cnt = 0;
			?>
			
			<div class="row">
				<?php foreach($posts as $post) : setup_postdata($post); $cnt++; ?>

					<div class="col-sm-12">
						<div style="border: 1px solid #ccc; background-color: white; margin-bottom: 20px; padding: 15px;">
							<div class="row">
								<div class="col-sm-6">
									<?php the_post_thumbnail('good-to-know-size', array('class' => 'img-responsive linkbox-img')); ?>
								</div>

								<div class="col-sm-6">
									<h2 style="text-transform: uppercase; margin-bottom: 20px; margin-top: 30px; padding-left: 15px; padding-right: 15px;"><?php the_title(); ?></h2>

									<div style="padding-left: 15px; padding-right: 15px; margin-bottom: 5px;">
										
										<div style="margin-top: 15px;"><?php the_content(); ?></div>

										<a href="<?php the_field('page_link') ?>" class="btn-primary" style="padding: 10px 65px; font-size: 16px;">
											Mer info
										</a>
									</div>

								</div>
							</div>
							
						</div>
							
					</div>

					<?php if($cnt % 4 == 0) : ?>
						<div class="hidden-xs clearfix"></div>
					<?php endif; ?>
				
				<?php 
				endforeach;
				wp_reset_postdata(); 
				?>
			</div>
		</div>
	</div>
</div>

<?php $url_2 = wp_get_attachment_url( get_field('bg_footer_top', 'options') ); ?>
<div style="background-image:url('<?php echo $url_2; ?>'); padding: 80px 0px;">
	<div class="container">
	
		<div class="row">
			<div class="col-sm-8">
				<h2 class="h2-big white" style="margin-top: 29px; margin-bottom: 0px;">
					Vad kostar våra tjänster?
					
				</h2>
			</div>

			<div class="col-sm-4">
				<a class="btn-primary pull-right btn-big" style="display: inline; line-height: 1.4; padding: 26px 30px;" href="/raknesnurra">Räkna ut ditt pris</a>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h2 class="h2-big text-center fancy" style="margin-top: 80px;"><span class="fancy-span">Nyheter</span></h2>
			<div class="row">
			<?php 
		    $instagram = @file_get_contents('http://instagram/2120755365');
		      if($instagram !== false)
		      {
		        $instagram = json_decode($instagram, true);
		        //var_dump($instagram);
		        $counter = 0;

		        foreach($instagram['data'] as $igrImage)
		        {
				$counter++;

				?>
		          
				<div class="col-sm-3" style="margin-bottom: 20px; max-width: 320px; margin: 0 auto; margin-bottom: 40px;">
					<div class="instaImage text-center" >
						<a class="fancybox" href="<?php echo $igrImage['images']['standard_resolution']['url']; ?>" title="<?php echo $igrImage['caption']['text']; ?>" rel="insta">
							<img src="<?php echo $igrImage['images']['standard_resolution']['url']; ?>" class="img-responsive" />		
						</a>
						<div class="instaText text-center" style="margin-top: 5px;">
							<?php echo $igrImage['caption']['text']; ?>
						</div>
					</div>						            							              
				</div>

		          <?php
		          if($counter == 4) : break; endif;
		        }
		      }
		    ?>

		    

		    <div class="clearfix" style=""></div>

		    <!-- <div class="col-sm-12 text-center" style="margin-bottom: 40px;">
		 		<a href="/nyheter-feed/" class="btn btn-primary" style="display: inline;">Se fler bilder</a>
		 	</div>		 -->
		 	</div>

		 	<div class="text-center" style=" margin-bottom: 40px;">
		    	<a href="/nyheter" class="btn-primary" style="padding: 10px 65px; font-size: 16px;">Visa fler</a>
		    </div>
		</div>
	</div>
</div>