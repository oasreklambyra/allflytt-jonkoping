<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

<div class="page-banner" style="background-image:url('<?php echo $url; ?>');">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 white">
				<h1 class="white mtop">
					<?php the_title(); ?>
				</h1>
				
				<article class="ingress white">
					<?php the_content(); ?>
				</article>
			</div>

			<?php include('move.php'); ?>
		</div>
	</div>
</div>

<div class="container">
	<div class="row page-content">
		<div id="calcApp" class="col-sm-12">
			
		</div>
	</div>
</div>
<div class="modal fade" id="sendRequestComplete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2 class="modal-title" id="myModalLabel" style="margin-bottom: 20px;margin-top: 30px;">Din offert-förfrågan har skickats.</h2>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" style="line-height:1;padding:10px 20px;">Ok</button>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo get_template_directory_uri(); ?>/dist/scripts/react/react.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/dist/scripts/react/JSXTransformer.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/dist/scripts/react/app.js"></script>