<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

<div class="start" style="background-image:url('<?php echo $url; ?>');">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">
				<h1 class="white mtop">
					Nyhetsarkiv
				</h1>

				<article class="ingress white">
					<?php the_content(); ?>
				</article>
			</div>

			<?php include('move.php'); ?>
		</div>
		
	</div>
</div>

<div class="container">
	<div class="col-sm-12">
		<h2 class="text-center">Nyheter</h2>
		<?php 
	    $instagram = @file_get_contents('http://instagram/2120755365');
	      if($instagram !== false)
	      {
	        $instagram = json_decode($instagram, true);
	        //var_dump($instagram);
	        $counter = 0;

	        foreach($instagram['data'] as $igrImage)
	        {
			$counter++;

			?>
	          
			<div class="col-sm-4">
				<div class="instaImage" >
					<a class="fancybox" href="<?php echo $igrImage['images']['standard_resolution']['url']; ?>" title="<?php echo $igrImage['caption']['text']; ?>" rel="insta">
						<img src="<?php echo $igrImage['images']['standard_resolution']['url']; ?>" class="img-responsive" />		
					</a>
					<div class="instaText text-center">
						<?php echo $igrImage['caption']['text']; ?>
					</div>
				</div>						            							              
			</div>

	          <?php
	          if($counter == 20) : break; endif;
	        }
	      }
	    ?>
	    <div class="clearfix" style="margin-bottom: 40px;"></div>					  
	</div>
</div>
