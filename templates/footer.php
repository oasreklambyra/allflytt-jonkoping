<footer id="contact" class="content-info" role="contentinfo">
	<?php $url_2 = wp_get_attachment_url( get_field('bg_footer_bottom', 'options') ); ?>

	<div class="start-contact-wrapper" style="background-image:url('<?php echo $url_2; ?>')">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="h2-big text-center mtop white fancy"><span class="fancy-span">Kontaktpersoner</span></h2>
					<?php $args = array(
					'posts_per_page'   => 2,
					'orderby'          => 'menu_order',
					'order'            => 'ASC',
					'post_type'        => 'contact',
					'post_status'      => 'publish' ); 
					
					$posts = get_posts($args);
					$cnt = 0;
					?>
					<div class="row">
						<?php foreach($posts as $post) : setup_postdata($post); $cnt++; ?>
							<div class="col-sm-6"> <?php //if($cnt == 2) : echo 'col-sm-offset-1'; endif; ?>
								<div class="image-wrapper">
									<div class="row">
										<div class="col-sm-4 p-right">
											<?php the_post_thumbnail('', array('class' => 'img-responsive')); ?>
										</div>

										<div class="col-sm-8 p-left">
											<div class="white-frame-box-2" style="height: 147px; position: static;">
												<strong class="mtop white-box-name"><?php the_title(); ?></strong>
												<p>
												<p class="white-box-tel" style="margin-top: 5px;"><?php the_field('tel'); ?></p>
												<p class="white-box-tel"><a href="mailto:<?php the_field('mail', 'options'); ?>"><?php the_field('mail', 'options'); ?></a></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php 
						endforeach;
						wp_reset_postdata(); 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php 

	$location = get_field('map', 'options');

	if( !empty($location) ):
	?>
	<div style="position: relative;">
		<div class="acf-map">
			<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
		</div>
		<?php endif; ?>

		<div class="container" style="position: absolute; top: 0; left: 0; right: 50px;">
			<div class="hidden-xs standard-content right" style="width: 220px; position: absolute;top: 30px;right: 0;background-color: white;padding: 15px; box-shadow: 5px 5px 10px -6px #000000;">
				<h2 class="find-us mtop">
					Hitta till oss
				</h2>

				<p style="margin-bottom: 0px;">
					<?php the_field('zip', 'options'); ?>
				</p>

				<a href="https://www.google.se/maps/place/Fordonsv%C3%A4gen+15,+553+02+J%C3%B6nk%C3%B6ping/@57.7440539,14.1611109,17z/data=!3m1!4b1!4m2!3m1!1s0x465a7269482a9c3b:0xd0f7efd1977d9ad6?hl=sv" target="blank" class="btn-primary" style="    padding: 10px;
    font-size: 16px;
    display: block;
    text-align: center;
    line-height: 1.3;
    margin-top: 20px;">
					Större karta
				</a>
			</div> 
		</div>
	</div>

	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyDouruFx8E1fYUdhHf1MB6YiNWOnbNl47M"></script>
	<!-- <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap" type="text/javascript"></script> -->
	<script type="text/javascript">


	(function($) {
	$('.btn-scroll').on('click', function(event) {
		event.preventDefault();
		var hash = $(this).attr('href');
		var offsetPixels = $(hash).offset().top - 100;
		$("html, body").animate({ scrollTop: offsetPixels  }, "slow");
	});
	/*
	*  render_map
	*
	*  This function will render a Google Map onto the selected jQuery element
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	$el (jQuery element)
	*  @return	n/a
	*/

	function render_map( $el ) {

		// var
		var $markers = $el.find('.marker');

		// vars
		var args = {
			zoom		: 16,
			center		: new google.maps.LatLng(0, 0),
			scrollwheel : false,
			draggable: false,
			zoomControl: true,
			disableDoubleClickZoom: true,
			streetViewControl: false,
			panControl: false,
			mapTypeId	: google.maps.MapTypeId.ROADMAP
		};

		// create map	        	
		var map = new google.maps.Map( $el[0], args);

		// add a markers reference
		map.markers = [];

		// add markers
		$markers.each(function(){

	    	add_marker( $(this), map );

		});

		// center map
		center_map( map );

	}

	/*
	*  add_marker
	*
	*  This function will add a marker to the selected Google Map
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	$marker (jQuery element)
	*  @param	map (Google Map object)
	*  @return	n/a
	*/

	function add_marker( $marker, map ) {

		// var
		var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

		// create marker
		var marker = new google.maps.Marker({
			position	: latlng,
			map			: map
		});

		// add to array
		map.markers.push( marker );

		// if marker contains HTML, add it to an infoWindow
		if( $marker.html() )
		{
			// create info window
			var infowindow = new google.maps.InfoWindow({
				content		: $marker.html()
			});

			// show info window when marker is clicked
			google.maps.event.addListener(marker, 'click', function() {

				infowindow.open( map, marker );

			});
		}

	}

	/*
	*  center_map
	*
	*  This function will center the map, showing all markers attached to this map
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	map (Google Map object)
	*  @return	n/a
	*/

	function center_map( map ) {

		// vars
		var bounds = new google.maps.LatLngBounds();

		// loop through all markers and create bounds
		$.each( map.markers, function( i, marker ){

			var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

			bounds.extend( latlng );

		});

		// only 1 marker?
		if( map.markers.length == 1 )
		{
			// set center of map
		    map.setCenter( bounds.getCenter() );
		    map.setZoom( 11 );
		}
		else
		{
			// fit to bounds
			map.fitBounds( bounds );
		}

	}

	/*
	*  document ready
	*
	*  This function will render each map when the document is ready (page has loaded)
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	5.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/

	$(document).ready(function(){

		$('.acf-map').each(function(){

			render_map( $(this) );

		});

	});

	})(jQuery);
	</script>

	<div class="container">
		<div class="row">
			<?php $args = array(
			'posts_per_page'   => -1,
			'orderby'          => 'menu_order',
			'order'            => 'DESC',
			'post_type'        => 'cert',
			'post_status'      => 'publish' ); 
			
			$posts = get_posts($args) ?>

			<?php foreach($posts as $post) : setup_postdata($post); ?>

				<div class="col-xs-6 col-sm-3 center-outer">
					<div class="center-inner">
						<?php if(get_field('embed')) : ?>
							<div class="trustpilot-widget" data-locale="sv-SE" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="5046a61e00006400051a876c" data-style-height="120px" data-style-width="100%" data-theme="light">
							    <a href="https://se.trustpilot.com/review/www.allflytt-jonkoping.se" target="_blank">Trustpilot</a>
							</div>

							<?php else : ?>

							<?php if(get_field('link_to')) : ?>
								<a href="<?php the_field('link_to') ?>" target="blank">
							<?php endif; ?>

							
								<?php the_post_thumbnail('cert-size', array('class' => 'img-responsive')); ?>
							

							<?php if(get_field('link_to')) : ?>
								</a>
							<?php endif; ?>
						<?php endif; ?>
					</div>
				</div>
			
			<?php endforeach;
			wp_reset_postdata(); ?>

			<div class="col-xs-6 col-sm-3 center-outer">
				<div class="center-inner">
					<div class="text-center contact-mid">
						<?php 
							$url = get_field('logo_footer', 'options');
							$img = wp_get_attachment_image_src( $url , 'full');
						?>

						<?php if(get_field('logo_link_to', 'options')) : ?>
							<a href="<?php the_field('logo_link_to', 'options') ?>" target="blank">
						<?php endif; ?>
							<img class="img-responsive" src="<?php echo $img[0]; ?>">
						<?php if(get_field('logo_link_to', 'options')) : ?>
							</a>
						<?php endif; ?>
					
</div>

					
				</div>
				
			</div>
<div class="col-xs-6 col-sm-3 center-outer">
				<div class="center-inner">
					<div class="text-center contact-mid">

<img src=https://www.allflytt-jonkoping.se/wp-content/uploads/2019/06/syna.png" alt="Syna" width="150">
</div>
</div>
			</div>
			<div class="col-xs-6 col-sm-3 center-outer">
				<ul class="center-inner">
					<li style="font-size: 16px;">Enkelt</li>
					<li style="font-size: 16px;">Effektivt</li>
					<li style="font-size: 16px;">Ansvarsfullt</li>

					<li style="font-size: 16px;list-style-type:none;">Org.nr: 556716-6839</li>
				</ul>

			</div>

		</div>
	</div>
</footer>