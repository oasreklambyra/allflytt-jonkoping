<?php //use Roots\Sage\Nav\NavWalker; ?>
<header class="banner" role="banner">
	<div class="container">
		<?php 
		$logo_id = get_field('logo', 'options');
		$logo_src = wp_get_attachment_image_src( $logo_id , 'full');
		?>

		<a class="brand hidden-xs" href="<?= esc_url(home_url('/')); ?>">
			<img src="<?php echo $logo_src[0]; ?>">
		</a>

		<span style="font-size: 22px; padding-left: 40px; color: #004ba1;">
			- Vi förenklar din flytt
		</span>
		
		<?php /*$link = get_field('link_contact', 'options');*/ ?>
		<span class="pull-right hidden-xs" style=" font-size: 18px; margin-top: 5px;">Ring oss direkt<br><strong>036 - 12 85 75</strong></span>
		
	</div>

	<div class="container">
		<nav class="navbar navbar-default navbar-fix-top" role="navigation">
			<a class="brand mobile-brand visible-xs" href="<?= esc_url(home_url('/')); ?>"><img src="<?php echo $logo_src[0]; ?>"></a>
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-meny-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<?php
		            wp_nav_menu( array(
		                'theme_location'    => 'primary_navigation',
		                'menu_class'        => 'nav navbar-nav',
		                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		                'walker'            => new wp_bootstrap_navwalker())
		            );
		        ?>

			</div>
			<div class="clearfix"></div>
		</nav>
	</div>
</header>
