<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

<div class="page-banner" style="background-image:url('<?php echo $url; ?>');">
	<div class="container">
		<div class="row">
			<div class="col-sm-7 white">
				<h1 class="white mtop"><?php the_title(); ?></h1>
				<article class="ingress"><?php the_field('ingress'); ?></article>
			</div>

			<div class="col-sm-1">
				
			</div>

			<?php include('move.php'); ?>
		</div>
	</div>
</div>

<div class="container">
	<div class="row page-content">
		<?php if($post->ID == 77) : ?>
			<?php $args = array(
			'posts_per_page'   => -1,
			'orderby'          => 'menu_order',
			'order'            => 'DESC',
			'post_type'        => 'relocating_tips',
			'post_status'      => 'publish' ); 
			
			$posts = get_posts($args);
			$cnt = 0; 
			?>
			
			<?php foreach($posts as $post) : setup_postdata($post); $cnt++; ?>
			
				<div class="col-sm-6">
					<div class="gray-frame">
						<div class="row">
							<div class="col-sm-4">
								<?php the_post_thumbnail('', array('class' => 'img-responsive')) ?>
							</div>

							<div class="col-sm-8">
								<h4><?php the_title(); ?></h4>
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				</div>

				<?php if($cnt % 2 == 0) : ?>
					<div class="clearfix"></div>
				<?php endif; ?>
			
			<?php endforeach;
			wp_reset_postdata(); ?>
			
		<?php else : ?>
			<div class="col-sm-6">
				<h2 class="mtop"><?php the_field('title_left'); ?></h2>
				<?php the_field('content'); ?>
			</div>

			<div class="col-sm-6">
				<h2 class="mtop"><?php the_field('title_right'); ?></h2>

				<?php the_field('content_right'); ?>
			</div>
		<?php endif; ?>
	</div>

	<div class="row">
		<div class="col-sm-12">

			<div class="row page-content-bottom">
				<?php
				if( have_rows('gallery_rep') ):
					while ( have_rows('gallery_rep') ) : the_row(); ?>
				
						<?php 
						$img = get_sub_field('img');
						$img = wp_get_attachment_image_src( $img , 'full'); 
						?>

						<div class="col-sm-3">
							<img class="img-responsive" src="<?php echo $img[0]; ?>">
						</div>

					<?php 
					endwhile;
				endif; ?>
			</div>
		</div>
	</div>
</div>