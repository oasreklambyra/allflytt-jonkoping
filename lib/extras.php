<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Config;

/**
 * Add <body> classes
 */
function body_class($classes) {
	// Add page slug if it doesn't exist
	if (is_single() || is_page() && !is_front_page()) {
		if (!in_array(basename(get_permalink()), $classes)) {
			$classes[] = basename(get_permalink());
		}
	}

	// Add class if sidebar is active
	if (Config\display_sidebar()) {
		$classes[] = 'sidebar-primary';
	}

	return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

add_image_size( 'cert-size', 150, 100, false );
add_image_size( 'services-size', 293, 200, true );
add_image_size( 'good-to-know-size', 454, 365, true );

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
	return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

function compile_css()
{
	$cssFile = get_template_directory() . '/dist/styles/main.css';
	$cssChange = filemtime($cssFile);

	$path = get_template_directory() .'/assets/styles';

	$dir = dir($path);
	$foundChanged = false;
	while (false !== ($entry = $dir->read()))
	{
		$file = $path . '/' . $entry;
		if(is_file($file)){
			$lessChange = filemtime($file);
			if($lessChange > $cssChange)
			{
				$foundChanged = true;
			}
		}
	}
	if($foundChanged)
	{
		exec('/usr/bin/node-sass '.$path .'/main.scss ' . $cssFile);
	}
}
add_action('init', __NAMESPACE__ . '\\compile_css');

if( function_exists('acf_add_options_sub_page') )
{
	acf_add_options_sub_page(array(
		'title' => 'Header',
		'capability' => 'manage_options'
	));
	acf_add_options_sub_page(array(
		'title' => 'Footer',
		'capability' => 'manage_options'
	));
	acf_add_options_sub_page(array(
		'title' => 'Länkar',
		'capability' => 'manage_options'
	));
	
}