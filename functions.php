<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/utils.php',                 // Utility functions
  'lib/init.php',                  // Initial theme setup and constants
  'lib/wrapper.php',               // Theme wrapper class
  'lib/conditional-tag-check.php', // ConditionalTagCheck class
  'lib/config.php',                // Configuration
  'lib/assets.php',                // Scripts and stylesheets
  'lib/titles.php',                // Page titles
  'lib/extras.php',                // Custom functions
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

require_once('wp_bootstrap_navwalker.php');

if ( isset($_POST['livingType']) && $_POST['livingType'] )
{
  function send_mail_from_calc()
  {
    add_filter('wp_mail_content_type', 'set_html_content_type' );

    // $to = 'info@allflytt-jonkoping.se'; 
    $to = 'perolov.nas@oas.nu'; 
    $subject = 'Offert-förfrågan av flytthjälp';

    $message = '<h2>Offert-förfrågan med dessa uppgifter:</h2>'.'<br/>';
    $message .= '<strong>Namn: </strong>'.$_POST['customerName'].'<br/>';
    $message .= '<strong>Adress: </strong>'.$_POST['customerAddress'].'<br/>';
    $message .= '<strong>Postnummer: </strong>'.$_POST['customerPostal'].'<br/>';
    $message .= '<strong>Ort: </strong>'.$_POST['customerCity'].'<br/>';
    $message .= '<strong>Vill ha svar via: </strong>'.($_POST['respondTo'] === 'both' ? 'Genom både Telefon och E-post' : ($_POST['respondTo'] === 'phone' ? 'Telefon' : 'E-post')).'<br/>';
    $message .= '<strong>Telefon: </strong>'.$_POST['customerPhone'].'<br/>';
    $message .= '<strong>E-post: </strong>'.$_POST['customerEmail'].'<br/>';
    $message .= '<br />';
    $message .= '<strong>Bostadstyp: </strong>'.getLivingType($_POST['livingType']).'<br/>';
    $message .= '<strong>Storlek: </strong>'.getSize($_POST['size']).'<br/>';
    $message .= '<strong>Våning: </strong>'.($_POST['level'] === 'moreThan7' ? 'Fler än 7' : $_POST['level']).'<br/>';
    $message .= '<strong>Hiss: </strong>'.getElevator($_POST['elevator']).'<br/>';
    $message .= '<strong>Flyttdatum: </strong>'.$_POST['movingDate'].'<br/>';
    $message .= '<strong>Boyta: </strong>'.$_POST['boyta'].'m<sup>2</sup>'.'<br/>';
    $message .= '<strong>Biyta: </strong>'.$_POST['biyta'].'m<sup>2</sup>'.'<br/>';
    $message .= '<strong>Bohag: </strong>'.getBohag($_POST['bohag']).'<br/>';
    $message .= '<br />';
    $message .= '<strong>Önskas flyttstädning: </strong>'.($_POST['movingHelp'] === '1' ? 'Ja' : 'Nej').'<br/>';
    $message .= '<strong>Önskas packningshjälp: </strong>'.($_POST['packingHelp'] === '1' ? 'Ja' : 'Nej').'<br/>';
    $message .= '<br />';
    if ($_POST['packingHelp'] === '1')
    {
     
      $both = false;
      $help = '';
      if (isset($_POST['packingHelpPacking']) && $_POST['packingHelpPacking'] === 'true')
      {
        $both = true;
        $help .= 'Packning';
      }
      if (isset($_POST['packingHelpUnpacking']) && $_POST['packingHelpUnpacking'] === 'true')
      {
        if ($both)
        {
          $help .= ', ';
        }
        $help .= 'Uppackning ';
      }
      if ($help)
      {
        $message .= '<strong>Typ av packningshjälp: </strong>'.'<br />';
        $message .= $help;
        $message .= '<br /><br />';
      }

      $both = false;
      $help = '';
      
      if (isset($_POST['packingHelpPackingPorcelain']) && $_POST['packingHelpPackingPorcelain'] === 'true')
      {
        $both = true;
        $help .= 'Packning';
      }
      if (isset($_POST['packingHelpUnpackingPorcelain']) && $_POST['packingHelpUnpackingPorcelain'] === 'true')
      {
        if ($both)
        {
          $help .= ', ';
        }
        $help .= 'Uppackning ';
      }
      if ($help)
      {
        $message .= '<strong>Endast porslin: </strong>'.'<br />';
        $message .= $help;
        $message .= '<br /><br />';
      }

      $message .= '<strong>Lån av kartonger: </strong>'.($_POST['packingHelpNeedBoxes'] === '1' ? 'Ja' : 'Nej').'<br/>';
      if ($_POST['packingHelpNeedBoxes'] === '1')
      {
        $message .= '<strong>Antal kartonger: </strong>'.$_POST['packingHelpBoxes'].'<br/>';
      }
      $message .= '<br />';
    }
    $message .= '<strong>Övrig information: </strong>'.$_POST['otherInfo'].'<br/>';

    $message .= '<strong>Magasinering: </strong>'.((isset($_POST['useMagazine']) && $_POST['useMagazine'] === 'true') ? 'Ja' : 'Nej').'<br/>';
    if (isset($_POST['useMagazine']) && $_POST['useMagazine'] === 'true')
    {

    }
    else 
    {
      $message .= '<br />';
      $message .= '<strong>Nya boendeuppgifter</strong><br />';
      $message .= '<strong>Ny adress: </strong>'.$_POST['newCustomerAddress'].'<br/>';
      $message .= '<strong>Nytt postnummer: </strong>'.$_POST['newCustomerPostal'].'<br/>';
      $message .= '<strong>Ny ort: </strong>'.$_POST['newCustomerCity'].'<br/>';
      $message .= '<strong>Ny bostadstyp: </strong>'.getLivingType($_POST['newLivingType']).'<br/>';
      $message .= '<strong>Våning: </strong>'.(($_POST['newLevel'] === 'moreThan7') ? 'Fler än 7' : $_POST['newLevel']).'<br/>';
      $message .= '<strong>Hiss:</strong> '.getElevator($_POST['newElevator']).'<br/>';
    }

    wp_mail( $to, $subject, $message );
    
    wp_mail( $_POST['customerEmail'], 'Bekräftelse offertförfrågan', $message );

    remove_filter('wp_mail_content_type', 'set_html_content_type');

    echo 'complete';
  }

  function set_html_content_type() 
  {
    return 'text/html';
  }

  function getLivingType($livingType)
  {
    $value = '';
    switch ($livingType) {
      case 'laganhet':
        $value = 'Lägenhet';
        break;
      case 'radhus':
        $value = 'Radhus';
        break;
      case 'villa':
        $value = 'Villa';
        break;
      case 'kontor':
        $value = 'Kontor';
        break;
      case 'magasin':
        $value = 'Magasin';
        break;
      case 'annat':
        $value = 'Annat';
        break;
    }
    return $value;
  }

  function getSize($size)
  {
    $value = '';
    switch ($size) {
      case '1':
        $value = '1 rum och kök';
        break;
      case '2':
        $value = '2 rum och kök';
        break;
      case '3':
        $value = '3 rum och kök';
        break;
      case '4':
        $value = '4 rum och kök';
        break;
      case '5':
        $value = '5 rum och kök';
        break;
      case '6+':
        $value = '6 eller fler rum';
        break;
    }
    return $value;
  }

  function getElevator($elevator)
  {
    $value = '';
    switch ($elevator) {
      case 'small':
        $value = 'Liten hiss';
        break;
      case 'large':
        $value = 'Stor hiss (4 pers eller fler)';
        break;
      case 'unknown':
        $value = 'Vet ej';
        break;
      default:
        $value = 'Nej';
    }
    return $value;
  }

  function getBohag($bohag)
  {
    $value = '';
    switch ($bohag) {
      case 'normalt':
        $value = 'Normalt';
        break;
      case 'sparsamt':
        $value = 'Sparsamt';
        break;
      case 'valmoblerat':
        $value = 'Välmöblerat';
        break;
    }
    return $value;
  }
  send_mail_from_calc();
}