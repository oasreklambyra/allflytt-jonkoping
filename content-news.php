<?php
/**
 * Template Name: Nyheter
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('templates/content', 'news'); ?>
<?php endwhile; ?>