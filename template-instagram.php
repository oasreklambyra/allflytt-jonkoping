<?php
/**
 * Template Name: Instagram
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('templates/content', 'instagram'); ?>
<?php endwhile; ?>